## Docker  
* [Instalacion docker](https://docs.docker.com/install/)  
* [Pasos despues instalacion](https://docs.docker.com/install/linux/linux-postinstall/)  
* [Docker hub](https://hub.docker.com/)  
---
* Trabajando con imagenes base  
`docker images`  
`docker search ubuntu`  
`docker pull ubuntu`  
`docker inspect imageId/imageName`
---
* Ejecutando contenedores  
`docker run hello-world:latest`  
`docker search whalesay`  
`docker run docker/whalesay cowsay holi`  
Que paso?  
[Repo](https://github.com/docker/whalesay)  
---
* Exponiendo contenedores  
`docker run -d redis` Como me conecto al servicio que esta corriendo en el contenedor?  
`docker run -d -p 6379:6379 redis`  
`docker ps`  
`redis-cli -h localhost -p 6379 ping`  

