### Construyendo contenedores  

* Ver folder app  
* Ver gunicorn.config.py  
* [Ver Dockerfile](https://docs.docker.com/engine/reference/builder/#from)
* Editar Dockerfile  
* Construir contenedor `docker build -t {imageTag} .`
* Correr la imagen
* curl localhost:8000
